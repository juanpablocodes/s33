// console.log("Hello")

// 3.
async function getList(){
	let result = await fetch("https://jsonplaceholder.typicode.com/todos");
	let json = await result.json();
	console.log(json);
	
// 4.
	let lists = json.map((json) => json.title)
	console.log(lists);
}

getList();

// 5.
fetch("https://jsonplaceholder.typicode.com/todos/4")
.then((response) => response.json())
.then((json) => console.log(json))

// 6.
fetch('https://jsonplaceholder.typicode.com/todos/103')
.then((response) => response.json())
.then((json) => {
    console.log(`title: ${json.title} | completed: ${json.completed}`);
});


// 7.
fetch("https://jsonplaceholder.typicode.com/todos", {
	method: "POST",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		userId: 1,
		completed: true,
		title: "New Post"
	})
})
.then((response) => response.json())
.then((json) => console.log(json))

// 8.
fetch("https://jsonplaceholder.typicode.com/todos/4", {
    method:"PUT",
    headers:{
        "Content-Type":"application/json"
    },
    body:JSON.stringify({
    title: "updated to do list"
    })
})
.then((response) => response.json())
.then((json) => console.log(json))

// 9.
fetch("https://jsonplaceholder.typicode.com/todos/4", {
    method:"PUT",
    headers:{
        "Content-Type":"application/json"
    },
    body:JSON.stringify({
    title: "updated to do list",
    description: "updated description",
    status: "completed",
    date_completed: '2022-08-26',
    userId: 777
    })
})
.then((response) => response.json())
.then((json) => console.log(json))

// 10.
fetch("https://jsonplaceholder.typicode.com/todos/4", {
    method:"PATCH",
    headers:{
        "Content-Type":"application/json"
    },
    body:JSON.stringify({
    title: "patched to do list",
    })
})
.then((response) => response.json())
.then((json) => console.log(json))

// 11.
fetch("https://jsonplaceholder.typicode.com/todos/4", {
    method:"PATCH",
    headers:{
        "Content-Type":"application/json"
    },
    body:JSON.stringify({
    completed: true,
    date_completed: '2022-08-26',
            
    })
})
.then((response) => response.json())
.then((json) => console.log(json))

// 12.
fetch("https://jsonplaceholder.typicode.com/todos/4", {
	method: "DELETE"
})
.then((response) => response.json())
.then((json) => console.log(json))
